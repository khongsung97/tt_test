<?php 
include_once 'tree.php';

if(isset($_POST['action']) && $_POST['action'] == 'huyen') {
	//echo $_POST['action'] . $_POST['id']; die;
	$id  = $_POST['id'];
	$str = json_decode($str, true);
	foreach($str as $key => $val) {
		if($key == $id) {
			$data = $val['quan_huyen'];
			echo json_encode(array_column($data, 'name_with_type', 'code'));
			break;
		}
	}
}

if(isset($_POST['action']) && $_POST['action'] == 'xa') {
	$idtp = $_POST['idtp'];
	$idhuyen = $_POST['idhuyen'];
	$str = json_decode($str, true);
	foreach($str as $key => $val) {
		if($key == $idtp) {
			$data = $val['quan_huyen'];
			foreach($data as $key => $val) {
				if($key == $idhuyen) {
					$xa = $val['xa_phuong'];
					echo json_encode(array_column($xa, 'name_with_type', 'code'));
				}
			}
		}
	}
}

