<?php 
include_once 'tree.php';

$str = json_decode($str, true);

/**
 * create file json province/
 */
if(!file_exists("./dist/tree.txt")) {
    $str = array_column($str, "name", "code");
    file_put_contents("./dist/tree.txt", json_encode($str));
    echo "Created the province json data tree";
} else {
    echo "province json data tree has been created!";
}

foreach($str as $key => $val) {
    $data = $val['quan_huyen'];
    $huyen = [];
    foreach($data as $value) {
        //create file json wards for each disctrict;
        if (!file_exists("./dist/xa-phuong/".$value['code'].".txt")) {
            file_put_contents("./dist/xa-phuong/".$value['code'].".txt",json_encode($value['xa_phuong']));
        }
        unset($value['xa_phuong']);
        $huyen[] = $value;
    }
    //create file json disctrict for each province;
    if(!file_exists("./dist/huyen/".$val['code'].".txt")) {
        file_put_contents("./dist/huyen/".$val['code'].".txt", json_encode($huyen));
    }
}

?>