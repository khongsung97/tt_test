(function($) {
	$.fn.helo = function(option) {
		var setting = $.extend({
			text: 'Hello world',
			color: null
		},option);

		console.log($(this).children().each(function() {
			$(this).text(setting.text);
			if (setting.color) {
				$(this).css("color", setting.color);
			}
			return this;
		}));
	}
})(jQuery);